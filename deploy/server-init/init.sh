#!/bin/sh -e

# Set locale
locale-gen en_GB.UTF-8

# Update packages
apt-get update -y
apt-get upgrade -y

# Install ohmyzsh
apt-get install -y zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Install DigitalOcean graphing
curl -sSL https://agent.digitalocean.com/install.sh | sh