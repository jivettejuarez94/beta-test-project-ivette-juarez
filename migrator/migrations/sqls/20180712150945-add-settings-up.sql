/* Replace with your SQL commands */

CREATE TABLE setting (
  id         VARCHAR(64) PRIMARY KEY,
  value      VARCHAR(512) NOT NULL UNIQUE,
  created_at TIMESTAMP   NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP   NOT NULL DEFAULT NOW()
);


INSERT INTO setting VALUES
  ('address', 'Travessera de Gràcia 68 4-2, Barcelona, Spain', NOW(), NOW()),
  ('phone', '+(34) 625 678 168 / +(34) 931 273 990', NOW(), NOW()),
  ('contactEmail', 'berta@bertamodels.com', NOW(), NOW()),
  ('facebook', 'https://www.facebook.com/bertamodels/', NOW(), NOW()),
  ('instagram', 'https://www.instagram.com/bertamodels/?hl=es', NOW(), NOW()),
  ('about', 'Berta is a model agency located in Barcelona, set up by model manager Berta Pons. We have over twenty-five years'' experience in model management and our philosophy lies in discovering young, fresh people and working hard to see them grow as people as well as models.', NOW(), NOW()),
  ('lateralText', 'Texto desde el backend.', NOW(), NOW());
