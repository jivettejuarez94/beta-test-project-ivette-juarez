import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ContentTypeService} from './content-type.service';
import {ContentService} from './content.service';
import {ContentTypeResolvers} from './content-type.resolvers';
import {ContentResolvers} from './content.resolvers';
import {ContentTypeEntity} from '../orm/entities/content-type.entity';
import {ContentEntity} from '../orm/entities/content.entity';
import {FileEntity} from '../orm/entities/file.entity';
import {SlugIsNotAlreadyTakenConstraint} from "./slug-exists.validator";
import {FileModule} from '@app/file/file.module';
import { ModelContentResolvers } from '@app/content/model-content.resolvers';
import {ContentController} from "@app/content/content.controller";
import { OldContentEntity } from '@app/orm/entities/old-content.entity';


@Module({
    imports: [
        TypeOrmModule.forFeature([
            ContentTypeEntity,
            ContentEntity,
            FileEntity,
            OldContentEntity
        ]),
        FileModule
    ],
    controllers: [ContentController],
    providers: [
        ContentTypeService,
        ContentService,
        ContentTypeResolvers,
        ContentResolvers,
        ModelContentResolvers,
        SlugIsNotAlreadyTakenConstraint
    ]
})
export class ContentModule {

}
