import {assignClean} from '@app/shared/utils';
import {ContentType} from '@app/graphql/schema';


export class ContentTypeDto extends ContentType {

    constructor(data: any){
        super();
        assignClean(this, data);
    }

}
