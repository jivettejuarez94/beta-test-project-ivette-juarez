import {assignClean} from '@app/shared/utils';
import {ContentTypeDto} from './content-type.dto';
import {UpsertContent} from '@app/graphql/schema';

export class UpsertContentDto extends UpsertContent {

    constructor(data: any){
        super();

        if(data && data.contentType) data.contentType = new ContentTypeDto(data.contentType);

        assignClean(this, data);
    }

    // @SlugIsNotAlreadyTaken({ always: true })
    slug?: string;
}
