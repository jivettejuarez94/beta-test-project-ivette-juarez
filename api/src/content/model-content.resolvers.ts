import { Resolver, ResolveProperty, Parent } from '@nestjs/graphql';
import { FileService } from '@app/file/file.service';
import { ModelContentDto } from '@app/content/dto/model-content.dto';
import { FileDto } from '@app/file/dto/file.dto';


@Resolver('ModelContent')
export class ModelContentResolvers {

  constructor(private readonly fileService: FileService) {
  }

  @ResolveProperty()
  async images(@Parent() content: ModelContentDto) {
    if (!content.images) return null;

    const files = await this.fileService.findByIds(content.images.map(i => +i.id));
    return files.map(f => f ? new FileDto(f) : null);
  }

}
