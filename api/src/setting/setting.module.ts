import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {SettingEntity} from "../orm/entities/setting.entity";
import {SettingService} from "./setting.service";
import {SettingResolvers} from "./setting.resolvers";

@Module({
    imports: [
        TypeOrmModule.forFeature([SettingEntity])
    ],
    providers: [
        SettingService,
        SettingResolvers,
    ],
    exports: [
        SettingService
    ]
})
export class SettingModule {
}
