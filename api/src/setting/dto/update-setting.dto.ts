import {IsNotEmpty, MaxLength} from 'class-validator';
import {assignClean} from '@app/shared/utils';
import {UpdateSetting} from '@app/graphql/schema';

export class UpdateSettingDto extends UpdateSetting {

    constructor(data: any) {
        super();
        assignClean(this, data);
    }

    @MaxLength(64)
    @IsNotEmpty()
    id: string;

    @MaxLength(64)
    @IsNotEmpty()
    value: string;

}
