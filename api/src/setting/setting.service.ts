import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {QueryFilter, QueryFilterModel} from '../shared/query-filter';
import {SettingEntity} from "../orm/entities/setting.entity";

@Injectable()
export class SettingService {

    constructor(
        @InjectRepository(SettingEntity)
        private readonly settingRepository: Repository<SettingEntity>,
    ){}

    async find(filter: QueryFilter): Promise<SettingEntity[]> {

        const { offset, limit, q} = filter;

        const builder = this.settingRepository.createQueryBuilder('s');

        if (q) {
            builder
                .where('s.id ~* :query')
                .orWhere('s.value ~* :query')
                .setParameters({ query: `.*${q}.*` });
        }

        return await builder
            .offset(offset)
            .limit(limit)
            .orderBy('id', 'ASC')
            .getMany();
    }

    async count(filter: QueryFilterModel): Promise<number> {

        const {q} = filter;

        const builder = this.settingRepository.createQueryBuilder('s');

        if (q) {
            builder
                .where('s.id ~* :query')
                .orWhere('s.value ~* :query')
                .setParameters({ query: `.*${q}.*` });
        }

        return await builder
            .orderBy('id', 'ASC')
            .getCount();
    }

    async findById(id: string): Promise<SettingEntity | undefined> {
        return this.settingRepository.findOne(id);
    }

    async updateById(id: string, entity: SettingEntity): Promise<SettingEntity | undefined> {
        const count = await this.settingRepository.count({ id });
        if (count === 0) return undefined;

        entity.updatedAt = new Date();

        await this.settingRepository.update(id, entity);

        return await this.findById(id);
    }

    async updateAll(settings: SettingEntity[]): Promise<SettingEntity[]> {

        // TODO make transactional

        const result: SettingEntity[] = [];

        for(const setting of settings) {
            const entity = await this.updateById(setting.id, setting);
            if(entity !== undefined) result.push(entity);
        }

        return result;
    }

}
