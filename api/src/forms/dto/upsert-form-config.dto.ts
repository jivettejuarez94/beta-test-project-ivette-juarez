import {assignClean} from '@app/shared/utils';
import {UpsertFormConfig} from '@app/graphql/schema';

export class UpsertFormConfigDto extends UpsertFormConfig {

    constructor(data: any) {
        super();
        assignClean(this, data);
    }
}
