import {assignClean} from '@app/shared/utils';
import {FormSubmission} from "@app/graphql/schema";

export class FormSubmissionDto extends FormSubmission {

    constructor(data: any){
        super();
        assignClean(this, data);
    }

}
