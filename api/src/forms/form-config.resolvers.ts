import { Args, Mutation, Query, Resolver, Parent, ResolveProperty } from '@nestjs/graphql';
import { UseGuards, ValidationPipe } from '@nestjs/common';
import { JwtAuthGuard } from '@app/auth/guards/jwt-auth.guard';
import { FormConfigService } from '@app/forms/form-config.service';
import { FormConfigDto } from '@app/forms/dto/form-config.dto';
import { UpsertFormConfigDto } from '@app/forms/dto/upsert-form-config.dto';
import { FormConfigEntity } from '@app/orm/entities/form-config.entity';
import {ModelContentDto} from "@app/content/dto/model-content.dto";
import {EmailConfigDto} from "@app/forms/dto/email-config.dto";

@Resolver('FormConfig')
export class FormConfigResolvers {
	constructor(private readonly formConfigService: FormConfigService) {}

	@Query()
	async formConfigs(@Args('offset') offset: number, @Args('limit') limit: number, @Args('q') q: string) {
		const entities = await this.formConfigService.find({ offset, limit, q });
		return entities.map(e => new FormConfigDto(e));
	}

	@Query()
	async formConfigsCount(@Args('offset') offset: number, @Args('limit') limit: number, @Args('q') q: string) {
		return await this.formConfigService.count({ offset, limit, q });
	}

	@Query()
	async formConfig(@Args('id') id: number) {
		const entity = await this.formConfigService.findById(id);
		return entity ? new FormConfigDto(entity) : null;
	}

	@ResolveProperty()
	emailConfig(@Parent() formConfig: FormConfigDto) {
		return new EmailConfigDto(formConfig.emailConfig);
	}

	@Mutation()
	@UseGuards(JwtAuthGuard)
	async createFormConfig(@Args('formConfig', new ValidationPipe()) dto: UpsertFormConfigDto) {
		const entity = new FormConfigEntity(dto);
		return new FormConfigDto(await this.formConfigService.create(entity));
	}

	@Mutation()
	@UseGuards(JwtAuthGuard)
	async updateFormConfigById(@Args('id') id: number, @Args('formConfig') dto: UpsertFormConfigDto) {
		const entity = new FormConfigEntity(dto);
		new FormConfigDto(await this.formConfigService.updateById(id, entity));
	}

	@Mutation()
	@UseGuards(JwtAuthGuard)
	async deleteFormConfigById(@Args('id') id: number) {
		return await this.formConfigService.deleteById(id);
	}
}
