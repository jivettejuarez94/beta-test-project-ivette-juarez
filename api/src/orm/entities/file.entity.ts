import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { assignClean } from '@app/shared/utils';
import { UserEntity } from '@app/orm/entities/user.entity';

@Entity('file')
export class FileEntity {

    constructor(data: any) {
        assignClean(this, data);
    }

    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ length: 128 })
    mimetype: string;

    @Column({ length: 128 })
    filename: string;

    @Column({ length: 32 })
    clName: string;

    @Column({ length: 32 })
    clPublicId: string;

    @Column({ length: 128 })
    clTransforms: string;

    @Column({ type: 'jsonb' })
    metadata: any;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    createdAt: Date;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    updatedAt: Date;

    @ManyToOne(type => UserEntity, user => user.id)
    owner: UserEntity;
}
