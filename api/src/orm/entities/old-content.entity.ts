import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {assignClean} from '../../shared/utils';

@Entity('jos_scatalog_products')
export class OldContentEntity {

    constructor(data: any) {
        assignClean(this, data);
    }

    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ length: 255 })
    title: string;

    @Column({ length: 255 })
    alias: string;

    @Column({ length: 255 })
    short_desc: string;

    @Column({ length: 255 })
    descr: string;

    @Column({ length: 255 })
    image: string;

    @Column()
    gallery_id: number;

    @Column()
    manufacturer_id: number;
    
    @Column({ length: 255 })
    metakey: string;
    
    @Column({ length: 255 })
    metadesc: string;
    
    @Column()
    ordering: number;
    
    @Column({ type: 'timestamp', default: () => null })
    created: Date;
    
    @Column()
    published: Boolean

    @Column()
    checked_out: number;

    @Column({ type: 'timestamp', default: () => null })
    checked_out_time: Date;
    
    @Column()
    recommended: boolean;
    
    @Column()
    hits: number;
    
    @Column({ length: 255 })
    default_image: string;
    
    @Column()
    in_town: boolean;

    @Column()
    new_face: boolean;

    @Column({ length: 255 })
    height: string;
    
    @Column({ length: 255 })
    bust: string;
    
    @Column({ length: 255 })
    waist: string;
    
    @Column({ length: 255 })
    hips: string;
    
    @Column({ length: 255 })
    shoe_size: string;
    
    @Column()
    hair_colour: number
    
    @Column()
    eye_colour: number;
    
    @Column()
    race: number;
    
    @Column({ length: 255 })
    pdf: string;
}
