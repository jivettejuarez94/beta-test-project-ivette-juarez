import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {ContentEntity} from '@app/orm/entities/content.entity';
import {assignClean} from '@app/shared/utils';

@Entity('content_type')
export class ContentTypeEntity {

    constructor(data: any) {
        assignClean(this, data);
    }

    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ length: 64 })
    name: string;

    @Column({ length: 64, nullable: true })
    slug: string;

    @Column({ type: 'jsonb' })
    formConfig: any;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    createdAt: Date;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    updatedAt: Date;

    @OneToMany(type => ContentEntity, content => content.contentType)
    content: ContentEntity[];
}
