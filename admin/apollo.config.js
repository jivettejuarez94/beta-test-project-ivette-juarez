module.exports = {

    schemas: {
        api: {
            schema: 'apollo/schemas/api.json',
            endpoint: 'http://api:3000/graphql'
        }
    },

    queries: [
        {
            schema: 'api',
            includes: ['**/*.graphql.ts'],
            excludes: ['node_modules/**']
        }
    ]

};
