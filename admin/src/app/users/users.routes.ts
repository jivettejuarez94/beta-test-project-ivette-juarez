import {UsersListComponent} from './users-list.component';
import {MainComponent} from '../layout/main.component';
import {AuthenticatedGuard} from '../auth/guards/authenticated.guard';
import {UserDetailComponent} from './user-detail.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

export const usersRoutes: Routes = [
  {
    path: 'users',
    component: MainComponent,
    canActivate: [AuthenticatedGuard],
    data: {
      title: 'Users'
    },
    children: [
      {
        path: ':id',
        component: UserDetailComponent,
        data: {
          title: 'User detail'
        }
      },
      {
        path: '',
        component: UsersListComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(usersRoutes)],
    exports: [RouterModule]
})
export class UsersRoutesModule {}
