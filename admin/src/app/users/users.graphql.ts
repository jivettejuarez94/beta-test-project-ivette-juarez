import gql from 'graphql-tag';


export const userListFragment = gql`
  fragment UserListView on User {
    id
    email
    firstName
    familyName
    createdAt
    updatedAt
  }
`;

export const userDetailFragment = gql`
  fragment UserDetailView on User {
    id
    email
    firstName
    familyName
    createdAt
    updatedAt
  }
`;

export const updateUserFragment = gql`
  fragment UpdateUserView on User {
    email
    firstName
    familyName
  }
`;

export const userListQuery = gql`
  query UserList($offset: Int, $limit: Int, $q: String){
    users(offset: $offset, limit: $limit, q: $q) {
      ...UserListView
    }
    usersCount(q: $q) 
  }
  ${userListFragment}
`;

export const userDetailQuery = gql`
  query UserDetail($id: ID!) {
    user(id: $id) {
      ...UserDetailView
    }  
  }
  ${userDetailFragment}
`;

export const deleteUserByIdMutation = gql`
  mutation DeleteUserById($id: ID!) {
    deleteUserById(id: $id) 
  }
`;

export const createUserMutation = gql`
  mutation CreateUser($user: UpsertUser) {
    createUser(user: $user) {
      ...UserDetailView 
    }
  }
  ${userDetailFragment}
`;

export const updateUserByIdMutation = gql`
  mutation UpdateUserById($id: ID!, $user: UpsertUser) {
    updateUserById(id: $id, user: $user) {
      ...UserDetailView
    }
  }
  ${userDetailFragment}
`;
