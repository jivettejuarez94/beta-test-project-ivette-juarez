import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/base.component';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorService } from '@app/error.service';
import { Observable, of } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { flatMap, map, tap } from 'rxjs/operators';
import { Apollo } from 'apollo-angular';
import { createUserMutation, updateUserByIdMutation, userDetailQuery } from '@app/users/users.graphql';
import { Location } from '@angular/common';
import { FetchResult } from 'apollo-link';
import { ApolloError } from 'apollo-client';
import { FormValidationService } from '@app/crud/form-validation.service';
import { cloneDeep } from 'apollo-utilities';
import { UserDetailView } from '@app/apollo/types/UserDetailView';
import { UserDetail } from '@app/apollo/types/UserDetail';

@Component({
  selector: 'app-user-detail',
  template: `
    <form [formGroup]="form" (ngSubmit)="onSubmit()">

      <formly-form [form]="form" [fields]="fields" [model]="model$ | async">
        <div class="pull-right">
          <button type="button" class="btn btn-default mr-sm-2" (click)="cancel()">
            <fa-icon icon="times" fixedWidth="true"></fa-icon>
            Cancel
          </button>
          <button type="submit" class="btn btn-primary" [disabled]="form.invalid">
            <fa-icon icon="save" fixedWidth="true"></fa-icon>
            Save
          </button>
        </div>
      </formly-form>
    </form>

    <ng-template #saveicon>
      <fa-icon icon="save" fixedWidth="true"></fa-icon>
    </ng-template>
  `
})
export class UserDetailComponent extends BaseComponent implements OnInit {

  form: FormGroup;

  model$: Observable<UserDetailView>;

  // fields

  id = {
    type: 'input',
    key: 'id',
    templateOptions: {
      label: 'Id',
      required: true,
      disabled: true
    }
  };

  email = {
    type: 'input',
    key: 'email',
    templateOptions: {
      label: 'Email',
      type: 'email',
      required: true
    },
    validation: {
      messages: {
        EmailIsNotAlreadyRegisteredConstraint: 'This email address is being used by another user'
      }
    },
    validators: {}
  };

  firstName = {
    type: 'input',
    key: 'firstName',
    templateOptions: {
      label: 'First Name',
      required: true
    }
  };

  familyName = {
    type: 'input',
    key: 'familyName',
    templateOptions: {
      label: 'Family Name',
      required: true
    }
  };

  password = {
    type: 'input',
    key: 'password',
    templateOptions: {
      label: 'Password',
      type: 'password'
    }
  };

  fields = [
    this.id,
    this.email,
    this.firstName,
    this.familyName,
    this.password
  ];

  private modelId: string;

  constructor(private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly apollo: Apollo,
    private readonly location: Location,
    private readonly formValidationService: FormValidationService,
    private readonly errorService: ErrorService) {
    super();
  }

  ngOnInit(): void {

    this.form = new FormGroup({});

    this.model$ = this.route.params
      .pipe(
        flatMap(params => {

          const { id } = params;
          this.modelId = id;

          if (id === 'new') {

            return of({} as UserDetailView);

          } else {

            return this.apollo
              .query<UserDetail>({
                query: userDetailQuery,
                variables: { id }
              })
              .pipe(map(({ data }) => {
                const { user } = data;
                // apollo freezes objects so we need to create a mutable copy
                return cloneDeep(user);
              }));

          }

        }),
        tap(user => this.form.reset(user))
      );

  }

  onSubmit() {

    const model = this.form.value;

    let observable: Observable<FetchResult<UserDetailView>>;

    if (this.modelId === 'new') {

      observable = this.apollo
        .mutate<UserDetailView>({
          mutation: createUserMutation,
          variables: {
            user: model
          }
        });

    } else {

      observable = this.apollo
        .mutate<UserDetailView>({
          mutation: updateUserByIdMutation,
          variables: {
            id: this.modelId,
            user: model
          }
        });

    }

    this.subscription(() => observable
      .subscribe(
        () => this.location.back(),
        (err) => {
          if (err instanceof ApolloError) {
            this.formValidationService.processErrors(err, this.form);
          } else {
            this.errorService.handleError(err);
          }
        }
      )
    );

  }

  cancel() {
    this.location.back();
  }


}
