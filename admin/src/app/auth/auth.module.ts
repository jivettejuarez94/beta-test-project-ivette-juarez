import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthenticatedGuard} from './guards/authenticated.guard';
import {TokenInterceptor} from './interceptors/token.interceptor';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {LoginComponent} from './login.component';
import {RouterModule} from '@angular/router';
import {authRoutes} from './auth.routes';
import {FormlyModule} from '@ngx-formly/core';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import {PasswordResetRequestComponent} from "./password-reset-request.component";
import {ResetPasswordComponent} from "./reset-password.component";
import {AuthService} from '@app/auth/auth.service';
import {StorageServiceModule} from 'ngx-webstorage-service';

@NgModule({
  imports: [
    CommonModule,
    StorageServiceModule,
    RouterModule.forChild(authRoutes),
    ReactiveFormsModule,
    FormlyModule,
    NgbAlertModule
  ],
  declarations: [
    LoginComponent,
    ResetPasswordComponent,
    PasswordResetRequestComponent
  ],
  providers: [
    AuthenticatedGuard,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class AuthModule {

}
