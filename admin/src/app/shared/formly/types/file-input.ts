import { FieldType } from "@ngx-formly/core";
import { ChangeDetectorRef, Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { Subscription } from 'rxjs/Subscription';
import { filter, map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Apollo } from 'apollo-angular';
import { fileByIdQuery } from '@app/shared/files.graphql';
import { FileById } from '@app/apollo/types/FileById';
import { FileDetailView } from '@app/apollo/types/FileDetailView';
import { environment } from '@src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'formly-file-input',
  template: `
    <div>
      <input type="hidden" [formlyAttributes]="field" [formControl]="formControl">

      <ng-container [ngSwitch]="(state$ | async)">

        <input *ngSwitchCase="'ready'" type="file" [accept]="to.accept" (change)="onChange($event)">

        <ng-container *ngSwitchCase="'generating_preview'">
          Generating preview...
          <fa-icon icon="spinner" fixedWidth="true" [spin]="true"></fa-icon>
        </ng-container>

        <ng-container *ngSwitchCase="'uploading'">
          Uploading...
          <fa-icon icon="spinner" fixedWidth="true" [spin]="true"></fa-icon>
        </ng-container>

        <ng-container *ngSwitchCase="'uploaded'">
          
          <ng-container [ngSwitch]="resourceType">
            
            <ng-container *ngSwitchCase="'video'">
              
              <video [poster]="previewUrl" controls width="800">
                <source *ngFor="let source of sources" [src]="source.url" [type]="source.type"/> 
              </video>
              
            </ng-container>

            <ng-container *ngSwitchCase="'image'">
              <img [src]="previewUrl" class="img-fluid" alt="Image Preview">
              <a [ngStyle]="{cursor: 'pointer', paddingLeft: '20px', transform: 'translateY(7px)', display: 'inline-block'}" (click)="editImage()">Edit image</a>
            </ng-container>
            
          </ng-container>

          <button *ngIf="!to['noClose']" type="button" class="close" style="opacity: 0.7" aria-label="Close" (click)="onDelete()">
            <span aria-hidden="true">&times;</span>
          </button>

        </ng-container>

        <ng-container *ngSwitchCase="'error'">
          {{ error }}
        </ng-container>

      </ng-container>

    </div>
  `
})
export class FormlyFileInput extends FieldType {

  previewUrl: string;
  error: Error;

  value$: Observable<any>;

  state$: BehaviorSubject<string>;

  resourceType: string;
  sources: { type: string, url: string }[];

  metadata: object;
  fileId: string;

  private subs: Subscription[];

  constructor(private http: HttpClient,
    private apollo: Apollo,
    private changeDetector: ChangeDetectorRef,
    private router: Router) {
    super();
  }

  ngOnInit(): void {

    super.ngOnInit();

    this.subs = [];
    this.state$ = new BehaviorSubject<string>('ready');

    const { formControl, subs } = this;
    this.value$ = formControl.valueChanges;

    subs.push(
      this.value$
        .pipe(filter(value => !value))
        .subscribe(_ => this.onReady())
    );

    subs.push(
      this.value$
        .pipe(filter(value => !!value))
        .subscribe(value => this.generatePreview(value))
    );

    // initial state

    const initialValue = formControl.value;

    if (initialValue) {
      this.generatePreview(initialValue);
    } else {
      this.onReady();
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.subs.forEach(s => s.unsubscribe());
  }

  onReady() {
    this.previewUrl = null;
    this.state$.next('ready');
    this.changeDetector.markForCheck();
  }

  onDelete() {
    this.formControl.setValue(null);
  }

  generatePreview(id: number) {

    const { state$ } = this;

    state$.next('generating_preview');

    this.subs.push(

      this.apollo.query<FileById>({
        query: fileByIdQuery,
        variables: { id }
      }).subscribe(
        ({ data }) => {

          const { file } = data;
          const { mimetype } = file;

          // TODO fix this hack for how metadata has string type instead of object
          const metadata = JSON.parse(JSON.stringify(file.metadata));

          // TODO reduce just to required data
          this.metadata = { ...metadata, modelId: this.model.id }; // to be removed
          this.fileId = file.id;

          const [resourceType] = mimetype.split('/');

          switch (resourceType) {

            case 'video':

              this.previewUrl = metadata.url_preview;

              this.sources = [
                { type: 'video/mp4', url: metadata.url_mp4 },
                { type: 'video/webm', url: metadata.url_webm },
                { type: 'video/ogg', url: metadata.url_ogg },
              ];

              break;


            case 'image':

              const secure_url = metadata.secure_url;
              const uploadPosition = secure_url.indexOf('upload') + 6;

              const urlFirstPart = secure_url.substring(0, uploadPosition);
              const urlLastPart = secure_url.substring(uploadPosition);

              const transforms = file.clTransforms || '';
              this.previewUrl = `${urlFirstPart}${transforms}${urlLastPart}`;


              break;
          }


          this.resourceType = resourceType;

          state$.next('uploaded');

        },
        err => {
          this.error = err;
          state$.next('error');
        },
        () => {
          // force detection of changes made from the subscription context
          this.changeDetector.markForCheck();
        }
      )
    );

  }

  onChange(event: any) {

    const { files } = event.target;

    if (files.length) {

      const [file] = files;

      this.state$.next('uploading');

      this.subs.push(
        this.uploadFile(file)
          .subscribe(
            file => this.formControl.setValue(file.id),
            err => {
              this.error = err;
              this.state$.next('error');
            }
          )
      );

    }

  }

  uploadFile(file: File): Observable<FileDetailView> {

    const reader = new FileReader();
    reader.readAsDataURL(file);

    let result = new Subject<FileDetailView>();

    reader.onabort = () => {
      result.error(new Error('Reader was aborted'));
    };

    reader.onerror = () => {
      result.error(reader.error);
    };

    reader.onload = () => {

      const formData = new FormData();
      formData.append('file', file, file.name);

      const url = environment.apiUrl + '/file';

      this.subs.push(
        this.http.post(url, formData)
          .pipe(map(resp => resp as FileDetailView))
          .subscribe(
            res => {
              result.next(res);
              result.complete();
            },
            err => result.error(err)
          )
      );

    };

    return result;
  }

  editImage() {
    this.router.navigate(['/content/cropper', this.fileId]);
  }

}
