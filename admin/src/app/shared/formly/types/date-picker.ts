import {Component} from '@angular/core';
import {FieldType} from '@ngx-formly/core';


@Component({
  selector: 'formly-date-picker',
  template: `
    <div class="input-group">
      <input [formlyAttributes]="field"
             [formControl]="formControl"
             [class.is-invalid]="showError"
             class="form-control"
             ngbDatepicker #d="ngbDatepicker">
      <div class="input-group-append">
        <button class="btn btn-outline-secondary" (click)="d.toggle()" type="button">
          <fa-icon icon="calendar" fixedWidth="true"></fa-icon>
        </button>
      </div>
    </div>
  `
})
export class FormlyDatePicker extends FieldType {

}
