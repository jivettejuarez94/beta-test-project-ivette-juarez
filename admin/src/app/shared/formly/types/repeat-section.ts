import { Component } from '@angular/core';
import { FieldArrayType, FormlyFormBuilder } from '@ngx-formly/core';

@Component({
  selector: 'formly-repeat-section',
  template: `
    <div class="row mt-3 mb-3">
      <div class="col">
        <button class="btn btn-primary float-right" type="button" (click)="add(0)">{{ field.fieldArray.templateOptions.btnText }}</button>
      </div>
    </div>
    <div *ngFor="let field of field.fieldGroup; let i = index;">
      <formly-group
        [model]="model[i]"
        [field]="field"
        [options]="options"
        [form]="formControl">
        <div class="col d-flex align-items-center">
          <button class="btn btn-link" style="color: #999" type="button" (click)="remove(i)">
            <fa-icon icon="times"></fa-icon>
          </button>
        </div>
      </formly-group>
    </div>
  `,
})
export class FormlyRepeatComponent extends FieldArrayType {
  constructor(builder: FormlyFormBuilder) {
    super(builder);
  }
}
