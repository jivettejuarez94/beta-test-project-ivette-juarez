import {NgModule} from '@angular/core';
import {FormControl, ReactiveFormsModule, ValidationErrors} from '@angular/forms';
import {FormlyFieldConfig, FormlyModule} from '@ngx-formly/core';
import {CommonModule} from '@angular/common';
import {PipesModule} from '../pipes/pipes.module';
import {NgbDatepickerModule, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import {FormlyRichText} from './types/rich-text';
import {QuillModule} from 'ngx-quill';
import {FormlyFieldSelect} from './types/select';
import {NgSelectModule} from '@ng-select/ng-select';
import {IconsModule} from '../../icons.module';
import {FormlyRepeatComponent} from './types/repeat-section';
import {FormlyDatePicker} from './types/date-picker';
import {FormlyFileInput} from './types/file-input';
import {ApolloModule} from 'apollo-angular';

export const FORMLY_CONFIG = {
  types: [
    { name: 'select', component: FormlyFieldSelect, extends: 'input' },
    { name: 'datepicker', component: FormlyDatePicker, extends: 'input' },
    { name: 'richtext', component: FormlyRichText, extends: 'textarea'},
    { name: 'repeat', component: FormlyRepeatComponent },
    { name: 'file', component: FormlyFileInput, extends: 'input' }
  ],
  validators: [
    { name: 'url', validation: UrlValidator },
    { name: 'email', validation: EmailValidator},
  ],
  validationMessages: [
    { name: 'url', message: UrlValidatorMessage },
    { name: 'email', message: EmailValidatorMessage },
    { name: 'required', message: RequiredValidatorMessage }
  ],
};

export function RequiredValidatorMessage(err, field: FormlyFieldConfig) {
  return `"${field.templateOptions.label || field.key}" is a required field`;
}

export function UrlValidator(control: FormControl): ValidationErrors {
  if (!control.value) return null;
  return /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/.test(control.value) ? null : { 'url': true };
}

export function UrlValidatorMessage(err, field: FormlyFieldConfig) {
  return `"${field.formControl.value}" is not a valid Url`;
}

export function EmailValidator(control: FormControl): ValidationErrors {
  if (!control.value) return null;
  return /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/.test(control.value) ? null : { 'email': true };
}

export function EmailValidatorMessage(err, field: FormlyFieldConfig) {
  return `"${field.formControl.value}" is not a valid email`;
}


@NgModule({
  imports: [
    FormlyModule,
    ReactiveFormsModule,
    CommonModule,
    PipesModule,
    NgbTypeaheadModule,
    NgbDatepickerModule,
    QuillModule,
    NgSelectModule,
    IconsModule,
    ApolloModule
  ],
  declarations: [
    FormlyFieldSelect,
    FormlyRichText,
    FormlyDatePicker,
    FormlyRepeatComponent,
    FormlyFileInput
  ]
})
export class FormlyTypesModule { }
