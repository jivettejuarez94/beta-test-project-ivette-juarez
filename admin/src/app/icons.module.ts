import {NgModule} from '@angular/core';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import {library} from '@fortawesome/fontawesome-svg-core';
import {
  faBriefcase, faCalendar,
  faChalkboard,
  faEdit,
  faFile,
  faHome,
  faNewspaper,
  faPlus, faSave,
  faSpinner,
  faTachometerAlt,
  faTags, faTimes,
  faTrash,
  faUsers,
  faCogs,
  faInfoCircle, faUpload
} from '@fortawesome/free-solid-svg-icons';
import {faCoffee} from '@fortawesome/free-solid-svg-icons/faCoffee';
import {faTwitter} from "@fortawesome/free-brands-svg-icons/faTwitter";

library.add(
  faUsers, faFile, faTachometerAlt, faCoffee, faHome, faBriefcase, faTags, faChalkboard,
  faTrash, faEdit, faPlus, faNewspaper, faSpinner, faSave, faTimes, faCalendar, faCogs,
  faInfoCircle, faTwitter, faUpload
);

@NgModule({
  imports: [
    FontAwesomeModule
  ],
  exports: [
    FontAwesomeModule
  ]
})
export class IconsModule {}
