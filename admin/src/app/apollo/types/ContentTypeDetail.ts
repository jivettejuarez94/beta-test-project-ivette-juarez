/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ContentTypeDetail
// ====================================================

export interface ContentTypeDetail_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
  slug: string | null;
  formConfig: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ContentTypeDetail {
  contentType: ContentTypeDetail_contentType | null;
}

export interface ContentTypeDetailVariables {
  id: string;
}
