/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SetAccessToken
// ====================================================

export interface SetAccessToken_setAccessToken {
  __typename: "AccessToken";
  id: string | null;
}

export interface SetAccessToken {
  setAccessToken: SetAccessToken_setAccessToken | null;
}

export interface SetAccessTokenVariables {
  id: string;
  expiresIn: number;
}
