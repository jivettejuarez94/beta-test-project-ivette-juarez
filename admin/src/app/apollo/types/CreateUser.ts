/* tslint:disable */
// This file was automatically generated and should not be edited.

import { UpsertUser } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: CreateUser
// ====================================================

export interface CreateUser_createUser {
  __typename: "User";
  id: string | null;
  email: string | null;
  firstName: string | null;
  familyName: string | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface CreateUser {
  createUser: CreateUser_createUser;
}

export interface CreateUserVariables {
  user?: UpsertUser | null;
}
