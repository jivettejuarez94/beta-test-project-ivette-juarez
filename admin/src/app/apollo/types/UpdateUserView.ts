/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: UpdateUserView
// ====================================================

export interface UpdateUserView {
  __typename: "User";
  email: string | null;
  firstName: string | null;
  familyName: string | null;
}
