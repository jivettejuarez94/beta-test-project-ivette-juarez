/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SettingDetail
// ====================================================

export interface SettingDetail_setting {
  __typename: "Setting";
  id: string | null;
  value: string | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface SettingDetail {
  setting: SettingDetail_setting | null;
}

export interface SettingDetailVariables {
  id: string;
}
