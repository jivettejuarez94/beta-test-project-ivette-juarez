/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteUserById
// ====================================================

export interface DeleteUserById {
  deleteUserById: boolean | null;
}

export interface DeleteUserByIdVariables {
  id: string;
}
