/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ContentList
// ====================================================

export interface ContentList_contentsAdmin_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
}

export interface ContentList_contentsAdmin {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  publishedAt: any | null;
  contentType: ContentList_contentsAdmin_contentType | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ContentList {
  contentsAdmin: (ContentList_contentsAdmin | null)[];
  contentsAdminCount: number;
}

export interface ContentListVariables {
  offset?: number | null;
  limit?: number | null;
  q?: string | null;
}
