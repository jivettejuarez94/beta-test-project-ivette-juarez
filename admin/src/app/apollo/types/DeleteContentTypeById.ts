/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteContentTypeById
// ====================================================

export interface DeleteContentTypeById {
  deleteContentTypeById: boolean | null;
}

export interface DeleteContentTypeByIdVariables {
  id: string;
}
