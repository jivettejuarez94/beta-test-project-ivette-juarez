/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: UserDetail
// ====================================================

export interface UserDetail_user {
  __typename: "User";
  id: string | null;
  email: string | null;
  firstName: string | null;
  familyName: string | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface UserDetail {
  user: UserDetail_user | null;
}

export interface UserDetailVariables {
  id: string;
}
