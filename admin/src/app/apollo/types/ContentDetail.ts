/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ContentDetail
// ====================================================

export interface ContentDetail_contentAdmin_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
  slug: string | null;
  formConfig: any | null;
}

export interface ContentDetail_contentAdmin {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  publishedAt: any | null;
  contentType: ContentDetail_contentAdmin_contentType | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ContentDetail {
  contentAdmin: ContentDetail_contentAdmin | null;
}

export interface ContentDetailVariables {
  id: string;
}
