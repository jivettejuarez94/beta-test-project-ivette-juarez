
const setAccessToken = async (obj, {id, expiresIn}, {cache}) => {

  const data = {
    accessToken: {
      __typename: 'AccessToken',
      id, expiresIn
    }
  };

  cache.writeData({data});

  return data;
};

const clearAccessToken = async (obj, args, {cache}) => {

  const data = {
    accessToken: undefined
  };

  cache.writeData({data});

  return true;
};

export const clientMutations = {
  setAccessToken, clearAccessToken
};
