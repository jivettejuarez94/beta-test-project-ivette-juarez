import { Component } from '@angular/core';
import { contentDetailQuery, deleteContentByIdMutation, modelListQuery } from '@app/content/content.graphql';
import { ModelContentListView } from '@app/apollo/types/ModelContentListView';

@Component({
  selector: 'app-model-list',
  template: `
    <app-list-view [listQuery]="listQuery"
                   [detailQuery]="detailQuery"
                   [deleteByIdMutation]="deleteByIdMutation"
                   [modelsParser]="modelsParser"
                   [countParser]="countParser"
                   [publishedSelect]="true">

      <ng-container *listHeader>
        <th scope="col" class="col-sm-1">Id</th>
        <th scope="col" class="col-sm-2">Email</th>
        <th scope="col" class="col-sm-2">Name</th>
        <th scope="col" class="col-sm-2">Published</th>
        <th scope="col" class="col-sm-2">Created</th>
        <th scope="col" class="col-sm-2">Last Updated</th>
        <th scope="col" class="col-sm-1">Actions</th>
      </ng-container>

      <ng-container *listItem="let model;">
        <td class="col-sm-1">{{ model.id }}</td>
        <td class="col-sm-2">{{ model.modelContent.email }}</td>
        <td class="col-sm-2">{{ model.modelContent.name }}</td>
        <td class="col-sm-2" [innerHTML]="renderPublished(model)"></td>
        <td class="col-sm-2">{{ model.createdAt | timeago:live }}</td>
        <td class="col-sm-2">{{ model.updatedAt | timeago:live }}</td>
      </ng-container>

    </app-list-view>
  `
})
export class ModelListComponent {

  listQuery = modelListQuery;
  detailQuery = contentDetailQuery;
  deleteByIdMutation = deleteContentByIdMutation;

  modelsParser(data: any): any[] {
    return data.contentsAdmin;
  }

  countParser(data: any): number {
    return data.contentsAdminCount;
  }

  renderPublished(model: ModelContentListView) {
    return model.publishedAt ?
      `<span class="badge badge-success">Published</span>` :
      `<span class="badge badge-secondary">Draft</span>`;
  }

}
