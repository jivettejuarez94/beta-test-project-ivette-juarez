import gql from 'graphql-tag';


export const contentTypeListFragment = gql`
  fragment ContentTypeListView on ContentType {
    id
    name
    slug
    formConfig
    createdAt
    updatedAt
  }
`;

export const contentTypeDetailFragment = gql`
  fragment ContentTypeDetailView on ContentType {
    id
    name
    slug
    formConfig
    createdAt
    updatedAt
  }
`;

export const contentTypeListQuery = gql`
  query ContentTypeList($offset: Int, $limit: Int, $q: String){
    contentTypes(offset: $offset, limit: $limit, q: $q) {
      ...ContentTypeListView
    }
    contentTypesCount(q: $q)
  }
  ${contentTypeListFragment}
`;

export const contentTypeDetailQuery = gql`
  query ContentTypeDetail($id: ID!) {
    contentType(id: $id) {
      ...ContentTypeDetailView
    }
  }
  ${contentTypeDetailFragment}
`;

export const deleteContentTypeByIdMutation = gql`
  mutation DeleteContentTypeById($id: ID!) {
    deleteContentTypeById(id: $id)
  }
`;
