import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/base.component';
import { FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import { filter, flatMap, map, tap } from 'rxjs/operators';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import {
  contentDetailBySlugQuery,
  contentDetailQuery,
  createContentMutation,
  updateContentByIdMutation,
} from '@app/content/content.graphql';
import { Apollo } from 'apollo-angular';
import { ApolloError } from 'apollo-client';
import { FormValidationService } from '@app/crud/form-validation.service';
import { ErrorService } from '@app/error.service';
import { cloneDeep } from 'apollo-utilities';
import { ToastrService } from 'ngx-toastr';
import { ContentDetailView } from '@app/apollo/types/ContentDetailView';
import { ContentDetailBySlug } from '@app/apollo/types/ContentDetailBySlug';
import { ContentDetail, ContentDetail_contentAdmin_contentType } from '@app/apollo/types/ContentDetail';
import { contentTypeListQuery } from '@app/content/content-type.graphql';
import { ContentTypeList } from '@app/apollo/types/ContentTypeList';
import { combineLatest } from 'rxjs';
import { FetchResult } from 'apollo-link';

@Component({
  selector: 'app-content-detail',
  template: `

    <form [formGroup]="form" (ngSubmit)="onSubmit()">

      <formly-form [form]="form" [fields]="fields" [model]="model$ | async"></formly-form>

      <formly-form [form]="dataForm" [fields]="dataFields" [model]="data">
        <div class="pull-right">
          <button type="submit" class="btn btn-primary mb-3" [disabled]="form.invalid || dataForm.invalid">
            <fa-icon icon="save" fixedWidth="true"></fa-icon>
            Save
          </button>
          <button *ngIf="!published" type="button" class="btn btn-primary mb-3 ml-2" 
                  [disabled]="form.dirty || dataForm.dirty"
                  (click)="onPublish()">
            <fa-icon icon="upload" fixedWidth="true"></fa-icon>
            Publish
          </button>
        </div>
      </formly-form>

    </form>

    <ng-template #saveicon>
      <fa-icon icon="save" fixedWidth="true"></fa-icon>
    </ng-template>
  `,
})
export class ContentDetailComponent extends BaseComponent implements OnInit {

  form: FormGroup;
  dataForm: FormGroup;

  modelId: string;

  model$: Observable<ContentDetailView>;

  data: any;

  contentType: any;

  // fields

  id = {
    type: 'input',
    key: 'id',
    templateOptions: {
      label: 'Id',
      disabled: true,
    },
  };

  createdAt = {
    type: 'input',
    key: 'createdAt',
    templateOptions: {
      label: 'Created At',
      disabled: true,
    },
  };

  updatedAt = {
    type: 'input',
    key: 'updatedAt',
    templateOptions: {
      label: 'Updated At',
      disabled: true,
    },
  };

  title = {
    type: 'input',
    key: 'title',
    templateOptions: {
      label: 'Title',
      required: true,
      type: 'text',
    },
  };

  meta = {
    key: 'meta',
    type: 'repeat',
    fieldArray: {
      fieldGroupClassName: 'row',
      templateOptions: {
        btnText: 'Add meta tag',
      },
      fieldGroup: [
        {
          key: 'key',
          type: 'input',
          className: 'col-3',
          templateOptions: {
            label: 'Name',
            required: true,
          },
        },
        {
          key: 'value',
          type: 'input',
          className: 'col-8',
          templateOptions: {
            label: 'Value',
            required: true,
          },
        },
      ],
    },
  };

  fields: FormlyFieldConfig[] = [
    this.id,
    this.createdAt,
    this.updatedAt,
    this.title,
    { template: `<h3>SEO</h3>` },
    this.meta,
    { template: `<h3 class="mt-3 mb-3">Content</h3>` },
  ];

  dataFields: FormlyFieldConfig[] = [];

  published: boolean;

  constructor(private readonly apollo: Apollo,
    private readonly location: Location,
    private readonly formValidationService: FormValidationService,
    private readonly toastrService: ToastrService,
    private readonly errorService: ErrorService,
    private readonly changeDetector: ChangeDetectorRef,
    private readonly route: ActivatedRoute) {
    super();
  }

  ngOnInit(): void {

    this.form = new FormGroup({});
    this.dataForm = new FormGroup({});

    this.data = {};

    const { apollo } = this;
    const { route } = this;
    const { params, data } = route;

    this.model$ = combineLatest(params, data)
      .pipe(
        flatMap(([params, data]) => {

          const { id } = params;
          const { slug, contentType } = data;

          // lookup by slug
          if (slug) {

            const queryRef = apollo
              .watchQuery<ContentDetailBySlug>({
                query: contentDetailBySlugQuery,
                variables: { slug },
                fetchPolicy: 'cache-and-network',
              });

            return queryRef.valueChanges
              .pipe(
                filter(({ data }) => !!data),
                map(({ data }) => cloneDeep(data.contentBySlugAdmin)),
              );
          }

          // lookup by id

          if (id && contentType) {

            if (id === 'new') {

              return apollo.query<ContentTypeList>({
                query: contentTypeListQuery,
                variables: { q: contentType },
              }).pipe(
                map(({ data }) => ({
                  id: 'new',
                  contentType: data.contentTypes[0] as ContentDetail_contentAdmin_contentType,
                  data: {}
                }) as ContentDetailView),
              );

            }

            const queryRef = this.apollo
              .watchQuery<ContentDetail>({
                query: contentDetailQuery,
                variables: { id },
                fetchPolicy: 'cache-and-network',
              });

            return queryRef.valueChanges
              .pipe(
                filter(({ data }) => !!data),
                map(({ data }) => cloneDeep(data.contentAdmin)),
              );
          }

        }),
        tap((content: ContentDetailView) => {

          this.form.reset(content);

          const { id, contentType, data, publishedAt } = content;
          const { formConfig } = contentType;

          this.dataFields = cloneDeep(formConfig['fields'] as FormlyFieldConfig[]);

          this.published = !!publishedAt;

          // allow content form group to update with new fields before trying to set them
          setTimeout(() => {
            this.dataForm.reset(data);
            this.data = data; // for some reason the repeat field only picks up existing data on load like this
            this.changeDetector.markForCheck(); // ensure form validation status is picked up
          });

          this.modelId = id;
          this.contentType = contentType;

        }),
      );

  }

  onSubmit() {

    const { contentType, modelId } = this;
    const { title, meta, slug, publishedAt } = this.form.value;
    const data = this.dataForm.value;

    const content = {
      title,
      meta,
      slug: slug || this.slugify(title, undefined),
      publishedAt,
      contentTypeId: contentType.id,
      data,
    };

    let mutation: Observable<FetchResult<ContentDetailView>>;

    if (modelId === 'new') {

      mutation = this.apollo
        .mutate<ContentDetailView>({
          mutation: createContentMutation,
          variables: {
            content: content
          }
        });

    } else {

      mutation = this.apollo
        .mutate<ContentDetailView>({
          mutation: updateContentByIdMutation,
          variables: { id: this.modelId, content },
        });

    }

    this.subscription(() => mutation
      .subscribe(
        () => {
          // indicate success
          this.toastrService.success('Save was successful');
          this.location.back();

        },
        (err) => {
          if (err instanceof ApolloError) {
            this.formValidationService.processErrors(err, this.form);
          } else {
            this.errorService.handleError(err);
          }
        },
      ),
    );

  }

  slugify(text, separator) {
    text = text.toString().toLowerCase().trim();

    const sets = [
      { to: 'a', from: '[ÀÁÂÃÄÅÆĀĂĄẠẢẤẦẨẪẬẮẰẲẴẶ]' },
      { to: 'c', from: '[ÇĆĈČ]' },
      { to: 'd', from: '[ÐĎĐÞ]' },
      { to: 'e', from: '[ÈÉÊËĒĔĖĘĚẸẺẼẾỀỂỄỆ]' },
      { to: 'g', from: '[ĜĞĢǴ]' },
      { to: 'h', from: '[ĤḦ]' },
      { to: 'i', from: '[ÌÍÎÏĨĪĮİỈỊ]' },
      { to: 'j', from: '[Ĵ]' },
      { to: 'ij', from: '[Ĳ]' },
      { to: 'k', from: '[Ķ]' },
      { to: 'l', from: '[ĹĻĽŁ]' },
      { to: 'm', from: '[Ḿ]' },
      { to: 'n', from: '[ÑŃŅŇ]' },
      { to: 'o', from: '[ÒÓÔÕÖØŌŎŐỌỎỐỒỔỖỘỚỜỞỠỢǪǬƠ]' },
      { to: 'oe', from: '[Œ]' },
      { to: 'p', from: '[ṕ]' },
      { to: 'r', from: '[ŔŖŘ]' },
      { to: 's', from: '[ßŚŜŞŠ]' },
      { to: 't', from: '[ŢŤ]' },
      { to: 'u', from: '[ÙÚÛÜŨŪŬŮŰŲỤỦỨỪỬỮỰƯ]' },
      { to: 'w', from: '[ẂŴẀẄ]' },
      { to: 'x', from: '[ẍ]' },
      { to: 'y', from: '[ÝŶŸỲỴỶỸ]' },
      { to: 'z', from: '[ŹŻŽ]' },
      { to: '-', from: '[·/_,:;\']' }
    ];

    sets.forEach(set => {
      text = text.replace(new RegExp(set.from, 'gi'), set.to);
    });

    text = text.toString().toLowerCase()
      .replace(/\s+/g, '-')         // Replace spaces with -
      .replace(/&/g, '-and-')       // Replace & with 'and'
      .replace(/[^\w\-]+/g, '')     // Remove all non-word chars
      .replace(/\--+/g, '-')        // Replace multiple - with single -
      .replace(/^-+/, '')           // Trim - from start of text
      .replace(/-+$/, '');          // Trim - from end of text

    if ((typeof separator !== 'undefined') && (separator !== '-')) {
      text = text.replace(/-/g, separator);
    }

    return text;
  }

  onPublish() {
    this.form.value.publishedAt = new Date().getTime();
    this.onSubmit();
  }

}
