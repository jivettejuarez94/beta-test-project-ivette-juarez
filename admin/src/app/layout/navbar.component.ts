import {Component} from '@angular/core';
import {AuthService} from '@app/auth/auth.service';

@Component({
  selector: 'app-navbar',
  template: `
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" [routerLink]="">Berta Models Admin</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" [routerLink]="" (click)="authService.logout()">Sign out</a>
        </li>
      </ul>
    </nav>`
})
export class NavbarComponent {

  constructor(public authService: AuthService){}

}
