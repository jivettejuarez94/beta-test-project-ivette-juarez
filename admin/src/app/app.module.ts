import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.routes';
import {AuthModule} from './auth/auth.module';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import {FORMLY_CONFIG, FormlyTypesModule} from './shared/formly/formly-types.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LayoutModule} from './layout/layout.module';
import {DashboardComponent} from './dashboard.component';
import {UsersModule} from './users/users.module';
import {ToastrModule} from 'ngx-toastr';
import {ErrorService} from './error.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ContentModule} from './content/content.module';
import {TimeagoModule} from 'ngx-timeago';
import {IconsModule} from './icons.module';
import {CmsApolloModule} from './apollo/apollo.module';
import {SettingModule} from "./setting/setting.module";


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent
  ],
  imports: [
    LoggerModule.forRoot({level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.OFF}),
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AuthModule,
    ReactiveFormsModule,
    FormlyTypesModule,
    FormlyModule.forRoot(FORMLY_CONFIG),
    FormlyBootstrapModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot(),
    LayoutModule,
    TimeagoModule.forRoot(),
    IconsModule,
    UsersModule,
    ContentModule,
    CmsApolloModule,
    SettingModule
  ],
  providers: [
    ErrorService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {




}
