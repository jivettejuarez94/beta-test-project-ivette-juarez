import gql from 'graphql-tag';

export const settingDetailFragment = gql`
  fragment SettingDetailView on Setting {
    id
    value
    createdAt
    updatedAt 
  }
`;

export const settingListQuery = gql`
  query SettingList($offset: Int, $limit: Int, $q: String){
    settings(offset: $offset, limit: $limit, q: $q) {
      ...SettingDetailView
    }
    settingsCount(q: $q)
  }
  ${settingDetailFragment}
`;

export const settingDetailQuery = gql`
  query SettingDetail($id: ID!) {
    setting(id: $id) {
      id
      value
      createdAt
      updatedAt
    }
  }
`;

export const updateSettingsMutation = gql`
  mutation UpdateSettings($settings: [UpdateSetting]) {
    updateSettings(settings: $settings) {
      ...SettingDetailView
    }
  }
  ${settingDetailFragment}
`;
