import {Component, OnInit} from "@angular/core";
import {BaseComponent} from "../shared/base.component";
import {FormGroup} from "@angular/forms";
import {Observable} from "rxjs/Observable";
import {FormlyFieldConfig} from "@ngx-formly/core";
import {Apollo, QueryRef} from "apollo-angular";
import {Location} from "@angular/common";
import {FormValidationService} from "../crud/form-validation.service";
import {ToastrService} from "ngx-toastr";
import {ErrorService} from "../error.service";
import {ActivatedRoute} from "@angular/router";
import {map} from "rxjs/operators";
import {ApolloError} from "apollo-client";
import {cloneDeep} from "apollo-utilities";
import {settingListQuery, updateSettingsMutation} from "../setting/setting.graphql";
import {ContentDetailView} from '../apollo/types/ContentDetailView';
import {SettingList} from '../apollo/types/SettingList';


@Component({
  selector: 'app-settings',
  template: `
    <form [formGroup]="form" (ngSubmit)="onSubmit()">

      <formly-form [form]="form" [fields]="fields" [model]="model$ | async"></formly-form>

      <button type="submit" class="btn btn-primary" [disabled]="form.invalid || dataForm.invalid">
        <fa-icon icon="save" fixedWidth="true"></fa-icon>
        Save
      </button>

    </form>
  `
})
export class SettingsComponent extends BaseComponent implements OnInit {

  form: FormGroup;
  dataForm: FormGroup;

  modelId: string;

  model$: Observable<ContentDetailView>;

  data: any;

  contentType: any;

  // fields

  address = {
    type: 'textarea',
    key: 'address',
    templateOptions: {
      label: 'Address',
      rows: 3,
      required: true
    }
  };

  phone = {
    type: 'input',
    key: 'phone',
    templateOptions: {
      label: 'Phone',
      required: true
    }
  };

  contactEmail = {
    type: 'input',
    key: 'contactEmail',
    templateOptions: {
      label: 'Contact Email',
      required: true
    }
  };

  facebook = {
    type: 'input',
    key: 'facebook',
    templateOptions: {
      label: 'Facebook',
      required: true
    }
  };

  instagram = {
    type: 'input',
    key: 'instagram',
    templateOptions: {
      label: 'Instagram',
      required: true
    }
  };

  about = {
    type: 'textarea',
    key: 'about',
    templateOptions: {
      label: 'About',
      required: true,
      rows: 3
    }
  };

  lateralText = {
    type: 'textarea',
    key: 'lateralText',
    templateOptions: {
      label: 'Texto Lateral',
      required: true,
      rows: 3
    }
  };

  fields: FormlyFieldConfig[] = [
    this.address,
    this.phone,
    this.contactEmail,
    this.instagram,
    this.facebook,
    this.about,
    this.lateralText
  ];

  dataFields: FormlyFieldConfig[] = [];

  queryRef: QueryRef<SettingList>;

  constructor(private readonly apollo: Apollo,
              private readonly location: Location,
              private readonly formValidationService: FormValidationService,
              private readonly toastrService: ToastrService,
              private readonly errorService: ErrorService,
              private readonly route: ActivatedRoute) {
    super();
  }

  ngOnInit(): void {

    this.form = new FormGroup({});
    this.dataForm = new FormGroup({});
    this.data = {};


    this.queryRef = this.apollo.watchQuery<SettingList>({
      query: settingListQuery,
      variables: {limit: 100},
      fetchPolicy: 'network-only'
    });


    this.subscription(() =>

      this.queryRef.valueChanges.pipe(
        map(({data}) => {
 
          // clone so we can edit these objects
          const {settings, settingsCount} = data;

          return {
            settings: cloneDeep(settings),
            settingsCount
          };

        })
      ).subscribe(({settings, settingsCount}) => {

        const settingsObject = settings.reduce((memo, next) => {
          memo[next.id] = next.value;
          return memo
        }, {});

        this.form.reset(settingsObject);

      })
    );

  }

  onSubmit() {

    const {apollo} = this;
    const model = this.form.value;

    const settings = Object.keys(model).map(key => {
      return {id: key, value: model[key]};
    });

    this.subscription(() =>
      apollo.mutate({
        mutation: updateSettingsMutation,
        variables: {settings}
      }).subscribe(
        () => {
          // indicate success
          this.toastrService.success('Save was successful');
        },
        (err) => {
          if (err instanceof ApolloError) {
            this.formValidationService.processErrors(err, this.form);
          } else {
            this.errorService.handleError(err);
          }
        }
      )
    );

  }

}
