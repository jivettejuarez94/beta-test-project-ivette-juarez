/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: Route
// ====================================================

export interface Route_contentType {
  __typename: "ContentType";
  name: string | null;
  slug: string | null;
}

export interface Route {
  __typename: "Content";
  id: string | null;
  slug: string | null;
  contentType: Route_contentType | null;
}
