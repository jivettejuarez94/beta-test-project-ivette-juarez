/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SettingList
// ====================================================

export interface SettingList_settings {
  __typename: "Setting";
  id: string | null;
  value: string | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface SettingList {
  settings: (SettingList_settings | null)[];
  settingsCount: number;
}

export interface SettingListVariables {
  offset?: number | null;
  limit?: number | null;
  q?: string | null;
}
