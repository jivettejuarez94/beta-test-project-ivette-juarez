/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FormContentView
// ====================================================

export interface FormContentView_formContent {
  __typename: "FormContent";
  title: string | null;
  subtitle: string | null;
  buttonText: string | null;
  submitted: string | null;
}

export interface FormContentView {
  __typename: "Content";
  title: string | null;
  meta: any | null;
  data: any | null;
  formContent: FormContentView_formContent | null;
}
