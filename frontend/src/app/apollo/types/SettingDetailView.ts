/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SettingDetailView
// ====================================================

export interface SettingDetailView {
  __typename: "Setting";
  id: string | null;
  value: string | null;
  createdAt: any | null;
  updatedAt: any | null;
}
