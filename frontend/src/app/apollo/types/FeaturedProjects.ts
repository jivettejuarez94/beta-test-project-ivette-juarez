/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FeaturedProjects
// ====================================================

export interface FeaturedProjects_featuredProjects {
  __typename: "FeaturedProject";
  id: string | null;
  title: string | null;
  slug: string | null;
}

export interface FeaturedProjects {
  featuredProjects: (FeaturedProjects_featuredProjects | null)[] | null;
}
