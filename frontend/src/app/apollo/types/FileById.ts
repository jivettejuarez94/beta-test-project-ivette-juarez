/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FileById
// ====================================================

export interface FileById_file_owner {
  __typename: "User";
  id: string | null;
}

export interface FileById_file {
  __typename: "File";
  id: string | null;
  mimetype: string | null;
  filename: string | null;
  metadata: any | null;
  createdAt: any | null;
  updatedAt: any | null;
  owner: FileById_file_owner | null;
}

export interface FileById {
  file: FileById_file | null;
}

export interface FileByIdVariables {
  id: string;
}
