/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ModelsByGenderQuery
// ====================================================

export interface ModelsByGenderQuery_modelsByGender_modelContent_images {
  __typename: "File";
  id: string | null;
  mimetype: string | null;
  filename: string | null;
  metadata: any | null;
}

export interface ModelsByGenderQuery_modelsByGender_modelContent {
  __typename: "ModelContent";
  name: string | null;
  email: string | null;
  gender: string | null;
  country: string | null;
  inTown: boolean | null;
  height: number | null;
  bust: number | null;
  waist: number | null;
  hips: number | null;
  shoes: number | null;
  eyeColor: string | null;
  hairColor: string | null;
  instagram: string | null;
  images: (ModelsByGenderQuery_modelsByGender_modelContent_images | null)[] | null;
}

export interface ModelsByGenderQuery_modelsByGender {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  modelContent: ModelsByGenderQuery_modelsByGender_modelContent | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ModelsByGenderQuery {
  modelsByGender: (ModelsByGenderQuery_modelsByGender | null)[];
}

export interface ModelsByGenderQueryVariables {
  gender: string;
  firstLetter?: string | null;
}
