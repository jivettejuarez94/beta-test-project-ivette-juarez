/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: AboutContentDetailView
// ====================================================

export interface AboutContentDetailView_contentType {
  __typename: "ContentType";
  name: string | null;
}

export interface AboutContentDetailView_aboutContent_people {
  __typename: "Person";
  name: string | null;
  position: string | null;
  email: string | null;
}

export interface AboutContentDetailView_aboutContent_coverImageFile {
  __typename: "File";
  id: string | null;
  mimetype: string | null;
  filename: string | null;
  metadata: any | null;
}

export interface AboutContentDetailView_aboutContent {
  __typename: "AboutContent";
  heading: string | null;
  people: (AboutContentDetailView_aboutContent_people | null)[] | null;
  coverImageFile: AboutContentDetailView_aboutContent_coverImageFile | null;
}

export interface AboutContentDetailView {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  contentType: AboutContentDetailView_contentType | null;
  aboutContent: AboutContentDetailView_aboutContent | null;
  createdAt: any | null;
  updatedAt: any | null;
}
