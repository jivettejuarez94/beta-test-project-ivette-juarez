import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { Router } from "@angular/router";
import { Apollo } from "apollo-angular";
import { ContentSelectItems, ContentSelectItems_contents } from "@app/apollo/types/ContentSelectItems";
import { Observable } from "rxjs";
import { contentSelectItemsQuery } from "@app/app.graphql";
import { map } from "rxjs/operators";

@Component({
  selector: 'app-model-select',
  template: `
    <ng-select
               [items]="items$ | async"
               (change)="onChange($event)"
               bindLabel="title">
    </ng-select>
  `
}) 
export class ModelSelectComponent implements OnInit {

  @Output() onModelSelected: EventEmitter<boolean> = new EventEmitter();

  items$: Observable<ContentSelectItems_contents[]>;

  constructor(private readonly router: Router,
    private readonly apollo: Apollo) { }

  ngOnInit(): void {

    const { apollo } = this;

    this.items$ = apollo
      .query<ContentSelectItems>({
        query: contentSelectItemsQuery,
        variables: {
          contentType: 'model'
        },
        fetchPolicy: 'network-only'
      }).pipe(map(({ data }) => data.contents));

  }

  onChange(event) {
    if (event) {
      this.onModelSelected.emit(true); // Emit event to inform listeners a model has been selected
      const { slug } = event;
      this.router.navigate(['/', slug]);
    }
  }

}
