import { Component, OnInit, Inject, HostListener, Input } from '@angular/core';
import { WINDOW } from '@app/window.service';
import { ResponsiveService } from '@app/responsive.service';
import { SelectionCookieService } from "@app/shared/services/selection-cookie.service";

@Component({
  selector: 'app-navbar',
  template: `
    <navbar-desktop [ngClass]="scrollPosition ? 'scrollPosition' : ''" *ngIf="!isMobile" class="navbar-desktop sticky-top" [numFavourites]="numFavourites" [modelsNameFavourites]="modelsNameFavourites">
      <nav>
        <ul class="nav">
          <li *ngFor="let path of paths" [routerLink]="path.routerLink" (click)="path.action ? path.action($event) : null">
            {{ path.label }}
          </li>
        </ul>
      </nav>
    </navbar-desktop>
    <navbar-mobile 
      *ngIf="isMobile" 
      class="navbar-mobile sticky-top" 
      [numFavourites]="numFavourites" 
      [modelsNameFavourites]="modelsNameFavourites" 
      (switchMenu)="handleSwitchMenu($event)" 
      [ngClass]="isCollapsed ? null : 'navbar-mobile-fixed'"
    >
      <nav>
        <ul class="nav">
          <li *ngFor="let path of paths" [routerLink]="path.routerLink" (click)="path.action($event)">
            {{ path.label }}
          </li>
        </ul>
      </nav>
    </navbar-mobile>
  `,
  styleUrls: ['navbar.component.sass'],
})
export class NavbarComponent implements OnInit {
  isMobile: any;
  @Input()
  isCollapsed: boolean = true;
  numFavourites: number = 0;
  modelsNameFavourites : String[];

  scrollPosition: boolean;
  
  paths: any[] = [
    { label: 'In Town', routerLink: '/' },
    { label: 'Women', routerLink: '/women' },
    { label: 'Men', routerLink: '/men' },
    { label: 'Contact', action: this.scrollToFooter }
  ];
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.setIsMobile();
  }

  constructor(
    @Inject(WINDOW) private readonly window: Window,
    private responsiveService: ResponsiveService,
    private cookieService: SelectionCookieService
  ) { }

  ngOnInit() {
    this.setIsMobile();
    this.numFavourites = this.cookieService.getSelectedIds().length;
    this.modelsNameFavourites = this.cookieService.getSelectedNames();
  
    // Subscribe to selection cookie changes
    this.cookieService.selectionChanged.subscribe((numFavourites: number) => {
      this.numFavourites = numFavourites;

      // Update menu link on mobile
      if (this.isMobile) {
        this.paths[4].label = `My Selection (${this.numFavourites})`;
      }
    });

    this.cookieService.selectionChangedNames.subscribe((modelsNameFavourites: String[]) => {
      this.modelsNameFavourites = modelsNameFavourites;
    });
    window.addEventListener('scroll', this.scroll, true); //third parameter

  }

  setIsMobile() {
    this.isMobile = this.responsiveService.getIsMobile();
    this.updateNavLinks();
  }

  updateNavLinks() {
    if (this.isMobile && this.paths.length < 5) {
      this.paths.push({ label: `My Selection (${this.numFavourites})`, routerLink: '/my-selection' });
    }
    if (!this.isMobile && this.paths.length === 5) {
      this.paths.splice(4, 1);
    }
  }

  scrollToFooter(event: Event) {
    event.preventDefault();
    const spaceHolder = document.getElementById('spaceHolder');
    if (spaceHolder) {
      return spaceHolder.scrollIntoView({ behavior: "smooth" });
    }
    const footer = document.getElementById('berta-footer');
    footer.scrollIntoView({ behavior: "smooth" });
  }

  handleSwitchMenu(e: boolean) {
    this.isCollapsed = e;
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);  
  }

  scroll = (): void => {
    if(window.pageYOffset > 100){
      this.scrollPosition = true;
    } else {
      this.scrollPosition = false;
    }
 };

}
