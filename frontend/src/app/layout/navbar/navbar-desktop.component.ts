import { Component, Input } from '@angular/core';
import { animate, style, transition, trigger } from "@angular/animations";

@Component({
  selector: 'navbar-desktop',
  template: `
    <div>
      <a (click)="onSearchClicked()" class="search-icon-link">
        <img src="/assets/img/search.svg" class="search.svg" [ngClass]="showSearch ? 'no-hover' : null" />
      </a>
      <app-model-select (onModelSelected)="onModelSelected()" *ngIf="showSearch" @fade></app-model-select>
      <a routerLink="/my-selection" class="heart-link">
        <img
          src="/assets/img/heart.svg"
          class="heart"
        />
        <span class="fav-counter">({{ numFavourites }})</span>
        <span class="tooltip-my-selection">
          <p  *ngFor="let element of modelsNameFavourites">
            {{element}}
          </p> 
        </span>
      </a>
    </div>
    <ng-content></ng-content>
    <img src="/assets/img/logo.svg" class="logo" routerLink="/"/>
    
    
  `,
  styleUrls: ['navbar-desktop.component.sass'],
  animations: [
    trigger('fade', [
      transition(':enter', [
        style({ opacity: 0, minWidth: 0 }),
        animate('.2s', style({ opacity: 1, minWidth: '190px' })),
      ]),
      transition(':leave', [animate('.2s', style({ opacity: 0, minWidth: 0 }))]),
    ]),
  ],
})
export class NavbarDesktopComponent {

  @Input() numFavourites: number;
  @Input() modelsNameFavourites : String[];
  showSearch: boolean;

  constructor() { 
   
  }


  onSearchClicked() {
    this.showSearch = !this.showSearch;
  }

  onModelSelected() {
    this.showSearch = false;
  }

}
