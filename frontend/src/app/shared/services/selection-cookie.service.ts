import {EventEmitter, Injectable} from "@angular/core";
import {CookieService} from "ngx-cookie-service";
import {ToastrService} from "ngx-toastr";

@Injectable()
export class SelectionCookieService {

  selectionChanged: EventEmitter<number> = new EventEmitter();
  selectionChangedNames: EventEmitter<String[]> = new EventEmitter();

  constructor(private cookieService: CookieService, private toastr: ToastrService) {

  }

  getSelectedIds(): string[] {

    const cookie = this.cookieService.get('selectedIds');
    if (!cookie) return [];

    const data = JSON.parse(cookie);

    if (!data.ids) {
      this.toastr.error('Selected model ids not found in cookie.', 'Error');
    }

    return data.ids;

  }

  addSelectedId(id) : void {
    const ids = this.getSelectedIds();

    if (ids.indexOf(id) > -1) return;

    ids.push(id);
    const cookie = JSON.stringify({ids: ids});
    this.cookieService.set('selectedIds', cookie, 30);

    // Emit event to inform listeners selected ids have changed
    this.selectionChanged.next(ids.length);
  }

  removeSelectedId(id) : void {
    const ids = this.getSelectedIds();
    const idx = ids.indexOf(id);

    if (idx < 0) return; 

    ids.splice(idx, 1);

    const cookie = JSON.stringify({ids: ids});
    this.cookieService.set('selectedIds', cookie, 30);
    // Emit event to inform listeners selected ids have changed
    this.selectionChanged.next(ids.length);
  }

  getSelectedNames(): string[] {

    const cookie = this.cookieService.get('selectedNames');
    if (!cookie) return [];

    const data = JSON.parse(cookie);
    if (!data.names) {
      this.toastr.error('Selected model names not found in cookie.', 'Error');
    }

    return data.names;
  }

  addSelectedNames(name) : void {
    const names = this.getSelectedNames();

    if (names.indexOf(name) > -1) return;

    names.push(name);
    const cookie = JSON.stringify({names: names});
    this.cookieService.set('selectedNames', cookie, 30);

    this.selectionChangedNames.next(names);
  }

 
  removeSelectedName(name) : void {
    const names = this.getSelectedNames();
    const namex = names.indexOf(name);

    if (namex < 0) return; 

    names.splice(namex, 1);

    const cookie = JSON.stringify({names: names});
    this.cookieService.set('selectedNames', cookie, 30);

    this.selectionChangedNames.next(names);
  }
} 
 