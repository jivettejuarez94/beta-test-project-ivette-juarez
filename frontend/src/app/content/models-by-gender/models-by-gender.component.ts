import { Component, HostListener, OnInit, Inject, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { animate, style, transition, trigger } from '@angular/animations';
import { ModelsByGenderQuery_modelsByGender, } from '@app/apollo/types/ModelsByGenderQuery';
import { ModelsByGenderQueryResult } from '@app/content/resolvers/models-by-gender.resolver';
import { combineLatest } from "rxjs";
import { ToastrService } from "ngx-toastr";
import Hammer from 'hammerjs';
import { WINDOW } from "@app/window.service";

@Component({
  selector: 'app-models-by-gender',
  templateUrl: 'models-by-gender.component.html',
  styleUrls: ['models-by-gender.component.sass'],
  animations: [
    trigger('fade', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('.5s', style({ opacity: 1 })),
      ]),
      transition(':leave', [animate('.2s', style({ opacity: 0 }))]),
    ]),
  ]
})
export class ModelsByGenderComponent implements OnInit, AfterViewInit {

  models: ModelsByGenderQuery_modelsByGender[];
  title: string;
  letters = [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z',
  ];
  newModelsArray: Array<any>;
  currentLetter: string;
  showLetters: boolean;
  hammer: Hammer;

  private screenWidth: any;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = window.innerWidth;
    this.shouldShowLetters();
  }
  arrowsColClass: string;
  bigLetterColClass: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly titleService: Title,
    private readonly metaService: Meta,
    private readonly router: Router,
    private readonly toastr: ToastrService,
    @Inject(WINDOW) private readonly window: Window
  ) { }

  ngOnInit(): void {

    // Get window size
    this.screenWidth = window.innerWidth;

    // Subscribe to route data and query params to update on queryParam changes
    const routeData$ = this.route.data;
    const queryParams$ = this.route.queryParams;

    combineLatest(routeData$, queryParams$).subscribe(([data, queryParams]) => {
      const { query } = data;

      this.currentLetter = queryParams['firstLetter'];

      const { content, modelsByGender } = query as ModelsByGenderQueryResult;
      const { title, meta } = content;

      this.models = modelsByGender as ModelsByGenderQuery_modelsByGender[];
      this.title = title;

      // SEO

      this.titleService.setTitle(`Berta Models - ${title}`);

      meta.forEach(m => {
        const { key, value } = m;
        this.metaService.updateTag({ name: key, content: value });
      });

      this.shouldShowLetters();

    },
      err => this.toastr.error('Failed to fetch models.', 'Error'));
  }

  ngAfterViewInit() {
    this.hammer = new Hammer(this.window.document.querySelector('.hammer-wrapper'));
    console.log(this.hammer);

  }

  insertLettersBetweenModels() {
    this.newModelsArray = [...this.models];

    this.letters.forEach(letter => {
      let index = this.newModelsArray.indexOf(
        this.models.find(
          m =>
            m.title.startsWith(letter.toUpperCase()) ||
            m.title.startsWith(letter.toLowerCase()),
        ),
      );

      if (index !== -1) {

        this.newModelsArray = [
          ...this.newModelsArray.slice(0, index),
          letter.toUpperCase(),
          ...this.newModelsArray.slice(index),
        ];
      }
    });

    if (!this.newModelsArray.length && this.currentLetter) { // if current letter is set, ensure letter is added to models
      this.newModelsArray.push(this.currentLetter.toUpperCase());
    }

  }

  onLetterSelected(letter: string) {

    if (letter.length > 1) {
      return this.toastr.error('Invalid letter selected: ' + letter, 'Error');
    }

    // If tablet or mobile, filter models by first letter
    if (this.screenWidth < 1024) {
      this.currentLetter = letter;
      this.router.navigate([], { queryParams: { firstLetter: letter } });

    } else { // Smooth scroll to start of section

      const element = document.getElementById(`model-letter-${letter.toUpperCase()}`);
      if (element) {

        const scrollTop = window.pageYOffset || element.scrollTop;
        const finalOffset = element.getBoundingClientRect().top + scrollTop - 40;

        window.parent.scrollTo({
          top: finalOffset,
          behavior: 'smooth'
        });
      }

    }

  }

  onPreviousLetter() {
    const nextIdx = this.currentLetter ? this.letters.indexOf(this.currentLetter) - 1 : this.letters.length - 1;
    this.currentLetter = nextIdx > -1 ? this.letters[nextIdx] : this.letters[this.letters.length - 1];
    this.router.navigate([], { queryParams: { firstLetter: this.currentLetter } });
  }

  onNextLetter() {
    const nextIdx = this.currentLetter ? this.letters.indexOf(this.currentLetter) + 1 : 0;
    this.currentLetter = nextIdx <= this.letters.length - 1 ? this.letters[nextIdx] : this.letters[0];
    this.router.navigate([], { queryParams: { firstLetter: this.currentLetter } });
  }

  shouldShowLetters() {

    if (this.screenWidth > 1023) {
      this.showLetters = true;
    } else {
      this.showLetters = false;
      if (!this.currentLetter) return this.onLetterSelected('a');
    }

    if (this.showLetters) {
      this.insertLettersBetweenModels();
    } else {
      this.initModelsWithNoLetters();
      this.setBigLetterNavigationClasses(this.screenWidth);
    }

    // Ensure models not filtered if letters are shown
    if (this.showLetters && this.currentLetter) {
      this.router.navigate([]);
    }

  }

  initModelsWithNoLetters() {

    this.newModelsArray = [...this.models];

  }

  setBigLetterNavigationClasses(screenWidth: number) {
    if (screenWidth < 362) {
      this.arrowsColClass = 'col-2';
      this.bigLetterColClass = 'col-8';
    } else {
      this.arrowsColClass = 'col-1';
      this.bigLetterColClass = 'col-10';
    }
  }

  onSwipeLeft = () => {
    this.onNextLetter();
  };

  onSwipeRight = () => {
    this.onPreviousLetter();
  };

}
