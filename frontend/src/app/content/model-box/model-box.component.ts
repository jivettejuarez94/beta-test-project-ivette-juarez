import {Component, Input, OnInit} from '@angular/core';
import {SelectionCookieService} from "@app/shared/services/selection-cookie.service";
import {ModelContentDetail_content} from "@app/apollo/types/ModelContentDetail";


@Component({
  selector: 'model-box',
  templateUrl: 'model-box.component.html',
  styleUrls: ['model-box.component.sass'],
})
export class ModelBoxComponent implements OnInit {
  @Input() model: ModelContentDetail_content;

  selected: boolean;
  selectedNames: boolean;

  constructor (
    private cookieService: SelectionCookieService
  ) {}

  ngOnInit(): void {
    const selectedIds = this.cookieService.getSelectedIds();
    const selectedNames = this.cookieService.getSelectedNames();
    this.selectedNames = selectedNames.indexOf(this.model.modelContent.name) > -1;
    this.selected = selectedIds.indexOf(this.model.id) > -1;

  }

  urlBgImg = img => `url(${img})`;

  onHeartClicked() : void {
    this.selected = !this.selected;

    const {id} = this.model;
    const name = this.model.modelContent.name;

    if (this.selected) {
      this.cookieService.addSelectedId(id);
      this.cookieService.addSelectedNames(name); 
    } else {
      this.cookieService.removeSelectedId(id);
      this.cookieService.removeSelectedName(name);
    }
  }
}
