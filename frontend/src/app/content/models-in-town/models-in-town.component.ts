import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { LayoutService } from '@app/layout/layout.service';
import { WINDOW } from '@app/window.service';

import { Apollo } from 'apollo-angular';
import { ToastrService } from 'ngx-toastr';
import { settingListQuery } from '@app/app.graphql';
import { SettingList } from '@app/apollo/types/SettingList';


import {
  ModelsInTownQuery, 
  ModelsInTownQuery_modelsInTown,
} from '@app/apollo/types/ModelsInTownQuery';

interface SettingsInterface {
  phone: string;
  contactEmail: string;
  address: string;
  about: string;
  facebook: string;
  instagram: string;
}

@Component({
  selector: 'app-models-in-town-content',
  templateUrl: 'models-in-town.component.html',
  styleUrls: ['models-in-town.component.sass'],
})
export class ModelsInTownComponent implements OnInit {
  modelsInTown: ModelsInTownQuery_modelsInTown[];
  showLateralText: boolean;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly titleService: Title,
    private readonly metaService: Meta,
    private readonly layoutService: LayoutService,
    @Inject(WINDOW) private readonly window: Window,
    private readonly apollo: Apollo,
    private readonly toastr: ToastrService
  ) {
    this.showLateralText = false;
  }

  settings: SettingsInterface;

  ngOnInit(): void {

    const { query } = this.route.snapshot.data;
    const { content, modelsInTown } = query as ModelsInTownQuery;
    const { title, meta } = content;

    this.modelsInTown = modelsInTown;

    // SEO

    this.titleService.setTitle(`Berta Models - ${title}`);

    meta.forEach(m => {
      const { key, value } = m;
      this.metaService.updateTag({ name: key, content: value });
    });

    this.window.scrollTo(0, 0);
  
  
    this.apollo.query<SettingList>({
      query: settingListQuery,
    }).subscribe(data => {

      const settings = data.data.settings;

      this.settings = (settings.reduce((memo, next) => {
        memo[next.id] = next.value;
        return memo
      }, {}) as SettingsInterface);

    }, err => this.toastr.error('Failed to load settings.', 'Error'));

  }

  showLateral() : void {
    this.showLateralText = !this.showLateralText;
  }
}
