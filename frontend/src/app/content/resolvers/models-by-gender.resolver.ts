import {Observable} from 'rxjs/Observable';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {contentByIdQuery, modelsByGenderQuery} from '@app/app.graphql';
import {flatMap, map} from 'rxjs/operators';
import {ModelsByGenderQuery, ModelsByGenderQuery_modelsByGender} from "@app/apollo/types/ModelsByGenderQuery";
import {ContentByIdQuery, ContentByIdQuery_content} from "@app/apollo/types/ContentByIdQuery";

export interface ModelsByGenderQueryResult {

  content: ContentByIdQuery_content | null
  modelsByGender: (ModelsByGenderQuery_modelsByGender | null)[]

}

@Injectable()
export class ModelsByGenderResolver implements Resolve<ModelsByGenderQueryResult> {

  constructor(private readonly apollo: Apollo){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ModelsByGenderQueryResult> {
    // TODO error handling

    const {contentId} = route.data;

    // Get firstLetter from query params if set
    const firstLetter = route.queryParamMap.get('firstLetter');

    return this.apollo.query<ContentByIdQuery>({
      query: contentByIdQuery,
      variables: { id: contentId},
    }).pipe(
      map(({data}) => data.content),
      flatMap(content => {

        const gender = content.data.gender || 'male';

        return this.apollo.query<ModelsByGenderQuery>({
          query: modelsByGenderQuery,
          variables: { gender, firstLetter }
        }).pipe(
          map(({data}) => {
            const modelsByGender = data.modelsByGender;
            return {
              content,
              modelsByGender
            };
          })
        )
      })
    );

  }



}
