import {Observable} from 'rxjs/Observable';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {contentByIdQuery, formConfigByIdQuery} from '@app/app.graphql';
import {flatMap, map} from 'rxjs/operators';
import {FormConfigByIdQuery_formConfig} from "@app/apollo/types/FormConfigByIdQuery";
import {FormContentByIdQuery, FormContentByIdQuery_content} from "@app/apollo/types/FormContentByIdQuery";
import {ToastrService} from "ngx-toastr";

export interface FormConfigByIdQueryResult {
  content: FormContentByIdQuery_content | null
  formConfig: FormConfigByIdQuery_formConfig | null
}

@Injectable()
export class FormResolver implements Resolve<FormConfigByIdQueryResult> {

  constructor(private readonly apollo: Apollo, private toastr: ToastrService){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FormConfigByIdQueryResult> {
    // TODO error handling

    const {contentId, contentType} = route.data;

    if (['form', 'my-selection'].indexOf(contentType) < 0) {
      this.toastr.error('Cannot resolve form content for content-type ' + contentType, 'Error');
    }

    return this.apollo.query<FormContentByIdQuery>({
      query: contentByIdQuery,
      variables: { id: contentId},
    }).pipe(
      map(({data}) => data.content),
      flatMap(content => {

        const id = content.data.formConfigId;

        if (!id) {
          this.toastr.error('Cannot resolve form config without formConfigId', 'Error');
        }

        return this.apollo.query<FormConfigByIdQueryResult>({
          query: formConfigByIdQuery,
          variables: { id }
        }).pipe(
          map(({data}) => {
            const formConfig = data.formConfig;
            return {
              content,
              formConfig
            };
          })
        )
      })
    );

  }



}
