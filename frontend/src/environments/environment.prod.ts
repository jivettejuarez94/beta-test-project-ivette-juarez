export const environment = {
  production: true,
  apiUrl: 'https://api.berta.production.beta.barcelona',
  contactEmail: 'hola@beta.barcelona'
};
